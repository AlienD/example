INSERT INTO subject (name, credit) VALUES ('Math analysis', 5);
INSERT INTO subject (name, credit) VALUES ('Advanced JAVA', 5);
INSERT INTO subject (name, credit) VALUES ('CyberSecurity', 6);
INSERT INTO subject (name, credit) VALUES ('Computer Architecture', 6);

INSERT INTO teacher (fullName, email, subjectID) VALUES ('Saule', 'saule2020@gmail.com', 1);
INSERT INTO teacher (fullName, email, subjectID) VALUES ('Olzhas', 'olki2020@gmail.com', 2);
INSERT INTO teacher (fullName, email, subjectID) VALUES ('Anna', 'anna2020@gmail.com', 3);
INSERT INTO teacher (fullName, email, subjectID) VALUES ('Shyngys', 'shyngys2020@gmail.com', 4);

