package kz.aitu.Spring.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "subject")
public class  Subject {
    @Id
    private int id;
    private String name;
    private int credit;

    public String getName() {
        return this.name;
    }

    public int getCredit() {
        return this.credit;
    }
}
