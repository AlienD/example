package kz.aitu.Spring.repository;

import kz.aitu.Spring.model.Subject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Integer> {
    @Query(value = "INSERT INTO subject (name, credit) VALUES  (:name, :credit)", nativeQuery = true)
    public void create(String name, int credit);
}
