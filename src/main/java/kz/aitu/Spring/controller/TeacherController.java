package kz.aitu.Spring.controller;

import kz.aitu.Spring.model.Teacher;
import kz.aitu.Spring.service.TeacherService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TeacherController {
    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping("/teachers")
    public ResponseEntity<?> getAll(){
        return teacherService.getAll();
    }

    @GetMapping("/teacher/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return teacherService.getByID(id);
    }

    @DeleteMapping("/teacherDel/{id}")
    public void deleteByID(@PathVariable int id){
        teacherService.deleteByID(id);
    }

    @PostMapping("/teacher")
    public ResponseEntity<?> create(@RequestBody Teacher teacher){
        return ResponseEntity.ok(teacherService.create(teacher));
    }
}
