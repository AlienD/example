package kz.aitu.Spring.controller;

import kz.aitu.Spring.model.Subject;
import kz.aitu.Spring.service.SubjectService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SubjectController {
    private final SubjectService subjectService;

    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @RequestMapping(value = "/subjects")
    public String getAll(Model model){
        List<Subject> subjects = subjectService.getAll();
        Subject newsubject = new Subject();
        model.addAttribute("newsubject", newsubject);
        model.addAttribute("subjects", subjects);
        return "subject";
    }

    @PostMapping("/register")
    public String Registration(@ModelAttribute("newsubject") Subject subject){
        subjectService.create(subject);
        return "success";
    }

    @GetMapping("/subject/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return subjectService.getByID(id);
    }

    @DeleteMapping("/subjectDel/{id}")
    public void deleteByID(@PathVariable int id){
        subjectService.deleteByID(id);
    }

    @PostMapping("/subject")
    public void create(@RequestBody Subject subject){
        subjectService.create(subject);
    }

    @PutMapping("/subject")
    public ResponseEntity<?> update(@RequestBody Subject subject){
        return ResponseEntity.ok(subjectService.save(subject));
    }
}
