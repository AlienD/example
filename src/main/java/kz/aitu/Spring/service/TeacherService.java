package kz.aitu.Spring.service;

import kz.aitu.Spring.model.Teacher;
import kz.aitu.Spring.repository.TeacherRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TeacherService {
    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(teacherRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(teacherRepository.findById(id));
    }

    public void deleteByID(int id){
        teacherRepository.deleteById(id);
    }

    public Teacher create(Teacher teacher){
        return teacherRepository.save(teacher);
    }
}
