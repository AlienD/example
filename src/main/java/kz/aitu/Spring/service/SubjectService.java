package kz.aitu.Spring.service;

import kz.aitu.Spring.model.Subject;
import kz.aitu.Spring.repository.SubjectRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubjectService {
    private final SubjectRepository subjectRepository;

    public SubjectService(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    public List<Subject> getAll(){
        return (List<Subject>) subjectRepository.findAll();
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(subjectRepository.findById(id));
    }

    public void deleteByID(int id){
        subjectRepository.deleteById(id);
    }

    public void create(Subject subject){
        subjectRepository.create(subject.getName(), subject.getCredit());
    }

    public Subject save(Subject subject){
        return subjectRepository.save(subject);
    }
}
